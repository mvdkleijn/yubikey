<?php
/*
 * Yubikey plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Yubikey plugin for Wolf CMS.
 *
 * The Yubikey plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
 */

/**
 * The Yubikey plugin allows end users to login to the site with their Yubikey.
 *
 * @package wolf
 * @subpackage plugin.yubikey
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */

Plugin::setInfos(array(
    'id'          => 'yubikey',
    'title'       => 'Yubikey',
    'description' => 'Allows your users to login using Yubikey.',
    'version'     => '1.0.0',
    'license'     => 'GPLv3',
    'author'      => 'Martijn van der Kleijn',
    'website'     => 'http://www.vanderkleijn.net/wolf-cms.html',
    'update_url'  => 'http://www.vanderkleijn.net/plugins.xml',
    'type'        => 'both',
    'require_wolf_version' => '0.6.0'
));

// Setup the controller.
Plugin::addController('yubikey', 'Yubikey', 'administrator', false);

// Setup observers
Observer::observe('login_required', 'yubikeyLogin');
//Observer::observe('logout_requested', 'openIdLogout');

// Load classes.
AutoLoader::addFile('Yubikey', PLUGINS_ROOT.'/yubikey/lib/Yubikey.php');

// Setup routes.
Dispatcher::addRoute(array(
    '/yubikey/login'       => '/plugin/yubikey/index',
    '/yubikey/login/:any'  => '/plugin/yubikey/index/$1'
   ));

// Event Handlers
function yubikeyLogin($uri) {
    echo 'TEST';
    Flash::set('redirect', $uri);
    redirect('http://localhost/wolfcms/yubikey/login');
}

function yubikeyLogout() {
    if (AuthUser::logout()) {
        Flash::set('success', __('Logout succeeded.'));
    }
}