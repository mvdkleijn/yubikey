<style>
.yubiKeyInput {
    /*
//background-color: white;
//background-image: url("./yubiright_16x16.gif");
background: url(<?php echo PLUGINS_URI.'yubikey/'; ?>images/login-bg.gif) no-repeat;
//background-repeat: no-repeat;
//background-attachment: scroll;
//background-position: 2px 2px;
//padding-left: 20px;
//height: 18px;
//width: 180px;
    */
    background: url(<?php echo PLUGINS_URI.'yubikey/'; ?>images/login-bg.gif) no-repeat;
    background-color: #fff;
    background-position: 0 50%;
    color: #000;
    padding-left: 18px;
    width: 220px;
    margin-right: 10px;
}
</style>


<div id="yubikey">
<form name="login" action="<?php echo $yubikey_submit; ?>" method="post" style="border: 1px solid #e5e5e5; background-color: #f1f1f1; padding: 10px; margin: 0px;"
	onSubmit="key.value = (key.value).toLowerCase(); return true;">
	<input type="hidden" name="mode" value="legacy">
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>

			<td width="150">
					<b>Username</b>
			</td>
			<td width="470">
				  <input autocomplete="off" type="text" name="username">
			</td>
		</tr>
		<tr>

			<td colspan=2>&nbsp;</td>
		</tr>
		<tr>
			<td  width="150">
				<b>Password</b>
			</td>
			<td width="470">
			  <input autocomplete="off" type="password" name="password">

			</td>
		</tr>
		<tr>
			<td colspan=2>&nbsp;</td>
		</tr>
		<tr>
			<td width="150">
				<b>YubiKey</b>

			</td>
			<td width="470">
			  <input autocomplete="off" type="text" name="key" class="yubiKeyInput">
              <input type="submit" value="Go" tyle="border: 0px; font-size: 0px; background: none; padding: 0px; margin: 0px; width: 0px; height: 0px;" />
			</td>
		</tr>
	</table>
	</form>
</div>