<?php
/*
 * Yubikey plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Yubikey plugin for Wolf CMS.
 *
 * The Yubikey plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
 */

/**
 * The Yubikey plugin allows end users to login to the site with their Yubikey.
 *
 * @package wolf
 * @subpackage plugin.yubikey
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */
?>

<p class="button"><a href="<?php echo get_url('plugin/yubikey/documentation'); ?>"><img src="<?php echo PLUGINS_URI; ?>yubikey/images/documentation.png" align="middle" /><?php echo __('Documentation'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/yubikey/settings'); ?>"><img src="<?php echo PLUGINS_URI; ?>yubikey/images/settings.png" align="middle" /><?php echo __('Settings'); ?></a></p>
<div class="box">
<h2><?php echo __('Yubikey plugin');?></h2>
<p>
    Homepage: <a href="http://www.vanderkleijn.net/wolf-cms/plugins/yubikey.html">Wolf CMS Yubikey plugin</a><br/>
    Homepage: <a href="http://www.wolfcms.org/">Wolf CMS</a>
</p>
</div>
