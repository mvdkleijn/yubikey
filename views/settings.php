<?php
/*
 * Yubikey plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Yubikey plugin for Wolf CMS.
 *
 * The Yubikey plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
 */

/**
 * The Yubikey plugin allows end users to login to the site with their Yubikey.
 *
 * @package wolf
 * @subpackage plugin.yubikey
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */
?>

<h1><?php echo __('Settings'); ?></h1>

<form action="<?php echo get_url('plugin/account/save'); ?>" method="post">
    <fieldset style="padding: 0.5em;">
        <legend style="padding: 0em 0.5em 0em 0.5em; font-weight: bold;"><?php echo __('General settings'); ?></legend>
        <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="label"><label for="settings[layout]"><?php echo __('Layout'); ?>: </label></td>
                <td class="field"><input class="textbox" id="settings[layout]" maxlength="255" name="settings[layout]" size="255" type="text" value="<?php echo $settings['layout']; ?>" /></td>
                <td class="help"><?php echo __('The page in the frontend menu that links to the forum.'); ?></td>
            </tr>
        </table>
    </fieldset>
    <p class="buttons">
        <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    </p>
</form>