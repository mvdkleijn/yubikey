<?php
/*
 * Yubikey plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the Yubikey plugin for Wolf CMS.
 *
 * The Yubikey plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/

/**
 * The Yubikey plugin allows end users to login to the site with their Yubikey.
 *
 * @package wolf
 * @subpackage plugin.yubikey
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */

class YubikeyController extends PluginController {

    function __construct() {
        AuthUser::load();
        if (defined('CMS_BACKEND')) {
            define('ACCOUNT_VIEWS', 'yubikey/views');
            $this->setLayout('backend');
        }
        else {
            define('ACCOUNT_VIEWS', '../../plugins/yubikey/views');
            $page = Page::findByUri(Plugin::getSetting('layout', 'yubikey'));
            $layout_id = $this->getLayoutId($page);

            $layout = Layout::findById($layout_id);
            $this->setLayout($layout->name);
        }
        $this->assignToLayout('sidebar', new View('../../plugins/yubikey/views/sidebar'));
    }

    private function getLayoutId($page) {
        if ($page->layout_id)
            return $page->layout_id;
        else if ($page->parent)
            return $this->getLayoutId($page->parent);
        else
            exit ('You need to set a layout!');
    }

    private function _checkLoggedIn() {
        if (!AuthUser::isLoggedIn())
            $this->display(ACCOUNT_VIEWS.'/403');
    }

    public function content($part=false, $inherit=false) {
        if (!$part)
            return $this->content;
        else
            return false;
    }

    public function index() {
//echo 'TEST-'.print_r(Flash::get('redirect'), true); exit();
        if ($_POST['key'] && $_POST['username']) {
            (isset($_POST['redirect'])) ? $redirect = $_POST['redirect'] : $redirect = false;
            $otp = strtolower ($_POST['key']);

            // $token_id would normally be retrived from db
            $token_id = md5 ("ccccccccjvnr:admin");

            if (md5 (substr ($otp, 0, 12).":".$_POST['username']) == $token_id) {
                //require_once "Yubikey.php";

                /******************************************************************************
                    Add your id and key to the variables below.
                    NOTE: The apiID is an integer and the signatureKey is a string.
                *******************************************************************************/

                $apiID = 4119;
                $signatureKey = 'aprtk1t5eaORLaUbwqTojDLsmVc=';

                /*****************************************************************************/

                $token = new Yubikey($apiID, $signatureKey);

                $token->setCurlTimeout(20);
                $token->setTimestampTolerance(500);

                //echo "<p>CURL Timeout: ".$token->getCurlTimeout()."</p>\n";
                //echo "<p>Timestamp Tolerance: ".$token->getTimestampTolerance()."</p>\n";

                if ($token->verify($otp)) {
                    
                    Flash::set('success', 'Successfully logged in.');
                    AuthUser::forceLogin($_POST['username']);
                    //echo 'success-'; exit;
                    if (false === $redirect) {
                        redirect(get_url());
                    } else {
                        redirect($redirect);
                    }
                }
                else {
                    //echo 'verify error'; exit;
                    Flash::set('error', 'Yubikey could not be verified.');
                    if (false === $redirect) {
                        redirect(get_url());
                    } else {
                        redirect($redirect);
                    }
                }

                //echo "\n<p>Response: ".$token->getLastResponse()."</p>\n";
            }
            else {
                //echo 'not yours'; exit;
                Flash::set('error', 'The Yubikey you presented does not match the Yubikey you registered.');
                if (false === $redirect) {
                    redirect(get_url());
                } else {
                    redirect($redirect);
                }
            }
        } else {
            $yubikey_submit = URL_PUBLIC.'yubikey/login';
//echo 'TEST'; exit();
            $this->display(ACCOUNT_VIEWS.'/login', array('yubikey_submit' => $yubikey_submit,
                                                         'redirect'       => Flash::get('redirect')
                                                        ));
        }
    }

    public function documentation() {
        $this->display(ACCOUNT_VIEWS.'/documentation');
    }

    /*
    public function settings() {
        $this->_checkLoggedIn();

        if (!User::hasPermission('administrator')) {
            $this->display(ACCOUNT_VIEWS.'/403');
        }

        $this->display(ACCOUNT_VIEWS.'/settings', array('settings' => Plugin::getAllSettings('account')));
    }
     *
    */

    /*
    function save() {
        if (isset($_POST['settings'])) {
            $settings = $_POST['settings'];
            foreach ($settings as $key => $value) {
                $settings[$key] = mysql_escape_string($value);
            }

            if (Plugin::setAllSettings($settings, 'account')) {
                Flash::set('success', __('The settings have been saved.'));
            }
            else {
                Flash::set('error', __('An error occured trying to save the settings.'));
            }
        }
        else {
            Flash::set('error', __('Could not save settings, no settings found.'));
        }

        redirect(get_url('plugin/forum/settings'));
    }
     *
    */
}